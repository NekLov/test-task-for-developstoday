from django.contrib import admin
from .models import (Event, UserFeedback, AdvancedUser, Photo,
                     Category, Oraganizator)

admin.site.register(Event)
admin.site.register(UserFeedback)
admin.site.register(AdvancedUser)
admin.site.register(Photo)
admin.site.register(Category)
admin.site.register(Oraganizator)
# Register your models here.
