from django.shortcuts import redirect, render_to_response, render,\
    get_object_or_404
from django.core.context_processors import csrf
from django.contrib.gis.geos import Point
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.gis.measure import D
from django.views.generic import View
from django.utils.decorators import method_decorator
from django.utils import timezone
from django.db import IntegrityError

from imhigh.models import (AdvancedUser, Event, Photo, UserFeedback, Category,
                           AuthorizationForm, SignUpForm, Oraganizator,
                           CategoriesForm)


def index(request):
    user = request.user
    if user.is_authenticated():
        index_data = {
            'label': "Log out",
            'urltext': "auth/logout",
            'user': user,
            'page_name': "I am High - Simple Event Service",
        }
    else:
        index_data = {
            'label': "Sign in",
            'urltext': "auth/login",
            'user': AdvancedUser(username="None"),
            'page_name': "I am High - Simple Event Service"
        }

    return render(request, 'imhigh/index.html', index_data)


class EventPage(View):
    @method_decorator(login_required(login_url='/auth/login'))
    def get(self, request, id):
        user = AdvancedUser.objects.get_by_natural_key(request.user.username)
        event = get_object_or_404(Event, id=id)
        mainphoto = Photo.objects.filter(fk=event, is_main=True)

        try:
            photo = mainphoto[0].photo.url
        except (IndexError):
            photo = '/media/photo/default.png'

        price = "It`s free(0$)" if event.event_amount is 0 else event.event_amount

        categories = event.categories.all()
        categories_list = [cat.value for cat in categories]
        separator = ","
        event_data = {
            'page_name': "I am High - Event Page",
            'user': user,
            'urltext': "/auth/logout",
            'label': "Log out",
            'event': event,
            'mainphoto': photo,
            'categories': separator.join(categories_list),
            'price': price,
            'organizator': event.event_organizator.user,
            'lng': str(event.event_place.x),
            'lat': str(event.event_place.y),
            'photos': Photo.objects.filter(fk=event)
        }
        event_data.update(csrf(request))

        return render_to_response('imhigh/eventpage.html', event_data)

    def post(self, request, id):
        user = AdvancedUser.objects.get_by_natural_key(request.user.username)
        event = Event.objects.get(id=id)
        event.user.add(user)

        return redirect("/auth/personal_page/"+user.username)


class CreateEvent(View):
    @method_decorator(login_required(login_url='/auth/login'))
    def get(self, request):
        user = AdvancedUser.objects.get(username=request.user.username)
        categories_form = CategoriesForm()
        event_data = {
            'page_name': "I am High - Create Event",
            'user': user,
            'urltext': "/auth/logout",
            'label': "Log out",
            'cat_form': categories_form
        }

        event_data.update(csrf(request))

        return render_to_response('imhigh/eventcreate.html', event_data)

    def post(self, request):

        lat = request.POST['lat']
        lng = request.POST['lng']
        try:
            organizator = Oraganizator(user=AdvancedUser.objects.get(
                username=request.user.username))
            organizator.save()
        except IntegrityError:
            organizator = Oraganizator.objects.get(
                user=AdvancedUser.objects.get(username=request.user.username))
        e = Event(
            event_name=request.POST['eventname'],
            event_description=request.POST['description'],
            event_begintime=request.POST['begintime'],
            event_endtime=request.POST['endtime'],
            event_amount=request.POST['price'],
            event_place=Point(float(lng), float(lat)),
            event_organizator=organizator,
        )
        e.save()
        categories_form = CategoriesForm(request.POST)
        if categories_form.is_valid():
            categories = categories_form.cleaned_data.values()[0]
        for cat in categories:
            e.categories.add(cat)
        files = request.FILES.getlist('photo')
        b = False
        for f in files:
            newphoto = Photo()
            newphoto.photo = f
            newphoto.fk = e
            if b is False:
                newphoto.is_main = True
                b = True
            newphoto.save()

        return redirect('/')


class FindEvent(View):
    @method_decorator(login_required(login_url='/auth/login'))
    def get(self, request):
        user = AdvancedUser.objects.get(username=request.user.username)
        category_form = CategoriesForm()
        page_info = {
            'page_name': "I am High - Find Event",
            'user': user,
            'urltext': "/auth/logout",
            'label': "Log out",
            'cat_form': category_form
        }

        page_info.update(csrf(request))

        return render_to_response('imhigh/findevent.html', page_info)

    def post(self, request):
        category_form = CategoriesForm(request.POST)
        category_form.is_valid()
        categories = category_form.cleaned_data.values()[0]
        begintime = request.POST['begintime']
        endtime = request.POST['endtime']
        lat = request.POST['lat']
        lng = request.POST['lng']
        point = Point(float(lng), float(lat))
        current_zoom = float(request.POST['zm'])
        MAX_ZOOM = 22
        radius = MAX_ZOOM - current_zoom

        events = Event.objects.filter(event_place__distance_lte=
                                     (point, D(km=radius)))

        events = events.filter(event_begintime__gte=begintime)

        events = events.filter(event_endtime__lte=endtime)

        events = events.filter(categories=categories)

        find_data = {
            'events': events,
            'user': request.user
        }

        find_data.update(csrf(request))

        return render_to_response("imhigh/findevent.html", find_data)


class SignIn(View):
    def get(self, request):
        form = AuthorizationForm()
        page_info = {
            'user': AdvancedUser(username='None'),
            'urltext': "/auth/login",
            'label': "Sign in",
            'page_name': "I am High - Sign In",
            'form': form,
        }
        page_info.update(csrf(request))
        return render_to_response('imhigh/loginpage.html', page_info)

    def post(self, request):
        #username = request.POST.get('username', '')
        #password = request.POST.get('password', '')
        form = AuthorizationForm(request.POST)
        if form.is_valid():
            user = authenticate(username=form.cleaned_data['username'],
                                password=form.cleaned_data['password'])
        if user is not None:
            login(request, user)
            return redirect('/')
        else:
            page_info = {
                'login_error': "User not found",
                'user': AdvancedUser(username='None'),
                'form': AuthorizationForm()
            }
            page_info.update(csrf(request))
            return render_to_response('imhigh/loginpage.html', page_info)


def signout(request):
    logout(request)
    return redirect("/")


class SignUp(View):

    def get(self, request):
        page_info = {
            'label': "Sign in",
            'urltext': "/auth/login",
            'user': AdvancedUser(username='None'),
            'page_name': "I am High - Sign Up",
            'form': SignUpForm()
        }
        page_info.update(csrf(request))
        return render_to_response('imhigh/signup.html', page_info)

    def post(self, request):
        page_info = {
            'label': "Sign in",
            'urltext': "/auth/login",
            'user': AdvancedUser(username='None'),
            'page_name': "I am High - Sign Up"
        }
        page_info.update(csrf(request))
        form = SignUpForm(request.POST)
        if form.is_valid():
            try:
                user = AdvancedUser.objects.get_by_natural_key(
                    form.cleaned_data['username'])
                page_info['login_error'] = "This user already exist"

                return render_to_response('imhigh/signup.html', page_info)

            except (KeyError, AdvancedUser.DoesNotExist):
                user = AdvancedUser(username=form.cleaned_data['username'],
                                    first_name=form.cleaned_data['first_name'],
                                    last_name=form.cleaned_data['last_name'],
                                    email=form.cleaned_data['email'])
                user.set_password(form.cleaned_data['password'])
                user.save()

        return redirect("/auth/login/")


class PersonalPage(View):

    @method_decorator(login_required(login_url='/auth/login'))
    def get(self, request, username):
        user = get_object_or_404(AdvancedUser, username=username)
        organized_events = Event.objects.filter(
            event_organizator=Oraganizator.objects.get(
                user=AdvancedUser.objects.get(username=username)))
        events = user.event_set.all()
        fedbacks = UserFeedback.objects.filter(feedback_target=user)
        personal_data = {
            'user': user,
            'org_events_list': organized_events,
            'part_events_list': events,
            'feedbacks': fedbacks,
            'label': "Log out",
            'urltext': "/auth/logout",
            'page_name': "I am High - Personal Page"
        }
        personal_data.update(csrf(request))

        return render_to_response('imhigh/personal_page.html', personal_data)

    def post(self, request, username):
        feedler_user = request.user.username
        target_user = AdvancedUser.objects.get_by_natural_key(username)
        text = request.POST['text']
        mark = request.POST['mark']
        feedback = UserFeedback(time=timezone.now(), feedler=feedler_user,
                                feedback_text=text, feedback_mark=mark,
                                feedback_target=target_user)
        feedback.save()

        feedbacks = UserFeedback.objects.filter(feedback_target=target_user)
        target_user.user_avgrate = sum(feedback.feedback_mark
                                       for feedback
                                       in feedbacks) / len(feedbacks)

        target_user.save()

        return redirect("/auth/personal_page/"+username)
