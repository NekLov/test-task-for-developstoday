from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings

from imhigh.views import EventPage, CreateEvent, FindEvent,\
    PersonalPage, SignIn, SignUp
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^event/create$', CreateEvent.as_view(), name='eventcreate'),
    url(r'^event/(?P<id>[0-9]+)/$', EventPage.as_view(),
        name='event_page'),
    url(r'^event/find$', FindEvent.as_view(), name='findevent'),
    url(r'^auth/login/', SignIn.as_view(), name='signin'),
    url(r'^auth/logout/', views.signout, name='signout'),
    url(r'^auth/signup/', SignUp.as_view(), name='signup'),
    url(r'^auth/personal_page/(?P<username>\w+)/$', PersonalPage.as_view(),
        name='personal_page'),
]
urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
