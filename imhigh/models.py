from django.contrib.gis.db import models
from django import forms

from django.contrib.auth.models import User


class AdvancedUser(User):
    user_avgrate = models.FloatField(default=0)


class Oraganizator(models.Model):
    user = models.OneToOneField(AdvancedUser)


class Category(models.Model):
    value = models.CharField(max_length=30, unique=True)

    def __str__(self):
        return self.value


class Event(models.Model):
    event_name = models.CharField(max_length=50)
    event_begintime = models.DateTimeField()
    event_endtime = models.DateTimeField()
    event_description = models.CharField(max_length=2048, null=True)
    event_amount = models.FloatField(default=0)
    event_place = models.PointField()
    event_organizator = models.ForeignKey(Oraganizator)
    event_avgrate = models.FloatField(default=0)
    user = models.ManyToManyField(AdvancedUser)
    categories = models.ManyToManyField(Category)
    objects = models.GeoManager()

    def __str__(self):
        return self.event_name


class UserFeedback(models.Model):
    feedler = models.CharField(max_length=30)
    feedback_text = models.CharField(max_length=2048)
    feedback_mark = models.FloatField()
    feedback_target = models.ForeignKey(AdvancedUser)
    time = models.DateTimeField(null=True)


class Photo(models.Model):
    photo = models.ImageField(upload_to="photo")
    fk = models.ForeignKey(Event)
    is_main = models.BooleanField(default=False)


class AuthorizationForm(forms.Form):
    username = forms.CharField(
        label="Username",
        max_length=30,
        widget=forms.TextInput(attrs={
            'class': "form-control",
            'placeholder': "Username",
            'required': "True",
        })
    )
    password = forms.CharField(
        label="Password",
        widget=forms.PasswordInput(attrs={
            'class': "form-control",
            'placeholder': "Password",
            'required': "True",
        })
    )


class SignUpForm(forms.Form):
    username = forms.CharField(
        label="Username",
        max_length=30,
        widget=forms.TextInput(attrs={
            'class': "form-control",
            'placeholder': "Username",
            'required': "True",
        })
    )
    first_name = forms.CharField(
        label="First name",
        max_length=30,
        widget=forms.TextInput(attrs={
            'class': "form-control",
            'placeholder': "First name",
            'required': "True",
        })
    )
    last_name = forms.CharField(
        label="Last name",
        max_length=30,
        widget=forms.TextInput(attrs={
            'class': "form-control",
            'placeholder': "Last name",
            'required': "True",
        })
    )
    email = forms.CharField(
        label="Email",
        max_length=30,
        widget=forms.TextInput(attrs={
            'class': "form-control",
            'placeholder': "Email",
            'required': "True",
            'type': "email",
        })
    )
    password = forms.CharField(
        label="Password",
        widget=forms.PasswordInput(attrs={
            'class': "form-control",
            'placeholder': "Password",
            'required': "True",
        })
    )
    confirm_password = forms.CharField(
        label="Confirm password",
        widget=forms.PasswordInput(attrs={
            'class': "form-control",
            'placeholder': "Confirm password",
            'required': "True",
        })
    )


class CategoriesForm(forms.ModelForm):

    class Meta:
        model = Event
        fields = ('categories',)

    def __init__(self, *args, **kwargs):
        super(CategoriesForm, self).__init__(*args, **kwargs)
        self.fields["categories"].widget = (
            forms.widgets.CheckboxSelectMultiple())
        self.fields["categories"].help_text = ""
        self.fields["categories"].queryset = Category.objects.all()
