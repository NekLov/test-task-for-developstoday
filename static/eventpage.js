var lng;
var lat;

window.onload = function () {
  var lati = document.getElementById("lat");
  var lngi = document.getElementById("lng");
  lng = parseFloat(lngi.value);
  lat = parseFloat(lati.value);

  geocoder = new google.maps.Geocoder();
  var latlng = new google.maps.LatLng(lat,lng);

  geocoder.geocode({'latLng': latlng}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      if (results[1]) {
        document.getElementById('adress').innerHTML = results[1].formatted_address;
      } else {
        alert('Can`t find adress');
      }
    } else {
      alert('Geocoder failed due to: ' + status);
    }
  });


  };
