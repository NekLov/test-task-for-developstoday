function validateForm () {
    var password1 = document.getElementById('id_password');
    var password2 = document.getElementById('id_confirm_password');
    if(password1.value.length < 6){
    	alert('Password is too short!')
    }
    if (password1.value !== password2.value) {
        alert('Passwords doesn`t match!');
        return false; 
    }
}
