var map;
var mark = new google.maps.Marker();
function initialize() {
  var mapOptions = {
    zoom: 14
  };
  map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);

  // Try HTML5 geolocation
  if(navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = new google.maps.LatLng(position.coords.latitude,
                                       position.coords.longitude);
          mark = new google.maps.Marker({
            position: pos,
            map: map
        });
        var lat = document.getElementById("lat");
        var lng = document.getElementById("lng");
        lat.value = pos.lat();
        lng.value = pos.lng();

      //var infowindow = new google.maps.InfoWindow({
        //map: map,
        //position: pos,
        //content: 'You are HERE!'
      //});

      map.setCenter(pos);
    }, function() {
      handleNoGeolocation(true);
    });

  } else {
    // Browser doesn't support Geolocation
    handleNoGeolocation(false);
  }
}

function handleNoGeolocation(errorFlag) {
  if (errorFlag) {
    var content = 'Error: The Geolocation service failed.';
  } else {
    var content = 'Error: Your browser doesn\'t support geolocation.';
  }

  var options = {
    map: map,
    position: new google.maps.LatLng(60, 105),
    content: content
  };

  var infowindow = new google.maps.InfoWindow(options);
  map.setCenter(options.position);
  
  
}

google.maps.event.addDomListener(window, 'load', initialize);

window.onload = function () {
  google.maps.event.addListener(map, 'click', function (e) {
    mark.setMap(null)
  var marker = new google.maps.Marker({
            position: e.latLng,
            map: map
        });
  mark=marker;
  var pos = e.latLng;
  var lat = document.getElementById("lat");
  var lng = document.getElementById("lng");

  lat.value = pos.lat();
  lng.value = pos.lng();
  });
  };
